#!/bin/bash
echo "Bonjour aujourd'hui nous allons calculer!"

echo -n "Donnes-moi un premier chiffre :"  $ch1
read ch1
while [[ $ch1 != +([0-9]) ]]
do 
    echo "Mauvaise saisie du chiffre."
    echo -n "Nouvelle tentative:"
    read ch1
done

echo "C'est tout bon!"
echo "Continuons"

echo -n "Donnes-moi un deuxi�me chiffre :" $ch2
read ch2
while [[ $ch2 != +([0-9]) ]]
do 
    echo "Mauvaise saisie du chiffre."
    echo -n "Nouvelle tentative:"
    read ch2
done
echo "C'est tout bon!"
echo "Continuons"

echo -n "Donnes-moi un op�rateur (a=addition, +=addition; s=soustraction, -=soustraction; m=multiplication, x=multiplication; et, d=division, /=division:" $op
read op

while [ $op != "a" ] && [ $op != "s" ] && [ $op != "m" ] && [ $op != "d" ] &&  [ $op != "+" ] && [ $op != "-" ] && [ $op != "/" ] && [ $op != "x" ]
do 
    echo "Mauvaise saisie du op."
    echo -n "Nouvelle tentative:"
    read op
done

case $op in
        "a" | "+") echo "le resultat est $(($ch1 + $ch2))" ;;
        "s" | "-") echo "le resultat est  $(($ch1 - $ch2))" ;;
        "m" | "x") echo "le resultat est $((ch1 * ch2))" ;;
        "d" | "/")
                if [ $ch2 = 0 ]; then
                        echo "pas 0"
                else
                        echo "le resultat est $(($ch1 / ch2))"
                fi ;;
        *) echo "Ce n'est pas le bon op�rateur, veuillez retentez votre chance!" ;;
esac